FROM debian:buster

# Speed up installs with eatmydata
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        eatmydata

###################
# ESP32 TOOLCHAIN #
###################

# https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/linux-setup.html
RUN eatmydata apt-get update && \
    eatmydata apt-get install -y --no-install-recommends \
        git \
        wget \
        flex \
        bison \
        gperf \
        python3 \
        python3-pip \
        python3-setuptools \
        cmake \
        ninja-build \
        ccache \
        libffi-dev \
        libssl-dev \
        dfu-util

# Python user installed packages
ENV PATH "${PATH}:${HOME}/.local/bin"
# Some scripts expect /usr/bin/python to be a thing
RUN ln -s /usr/bin/python3 /usr/bin/python

# Set up SDK - ESP-IDF
ENV IDF_TOOLS_PATH /opt/esp-idf-tools
ENV ESP_IDF_PATH /opt/esp-idf.git

# https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/index.html#get-started-get-esp-idf
# The branch is the latest release when writing this. Just to freeze it.
RUN git clone  \
    --recursive \
    --branch v4.2 \
    https://github.com/espressif/esp-idf.git \
    ${ESP_IDF_PATH}

# Set up Toolchain
RUN ${ESP_IDF_PATH}/install.sh

RUN echo "alias get_idf='. ${ESP_IDF_PATH}/export.sh'" >> /root/.bashrc

# Reduce image size
RUN rm -rf /var/lib/apt
